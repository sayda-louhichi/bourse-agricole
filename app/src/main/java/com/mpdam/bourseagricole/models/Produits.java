package com.mpdam.bourseagricole.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Produits implements Serializable {
    @SerializedName("id")
    private String id;

    @SerializedName("libelle")
    private String libelle;

    @SerializedName("categorie")
    private String categorie;

    @SerializedName("prix")
    private String prix;

    @SerializedName("quantite")
    private String quantite;

    @SerializedName("image")
    private String image;

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("id_user")
    private int id_user;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getPrix() {
        return prix;
    }

    public void setPrix(String prix) {
        this.prix = prix;
    }

    public String getQuantite() {
        return quantite;
    }

    public void setQuantite(String quantite) {
        this.quantite = quantite;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public Produits(String libelle, String prix) {
        this.libelle = libelle;
        this.prix = prix;
    }
}
