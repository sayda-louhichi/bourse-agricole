package com.mpdam.bourseagricole.adapters;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mpdam.bourseagricole.R;
import com.mpdam.bourseagricole.activities.HomeActivity;
import com.mpdam.bourseagricole.activities.UpdateProduitActivity;
import com.mpdam.bourseagricole.models.Produits;
import com.mpdam.bourseagricole.models.ServerResponse;
import com.mpdam.bourseagricole.webservices.RetrofitInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mpdam.bourseagricole.webservices.APIUtil.BASE_URL;

/**
 * adapter.
 */
public class NewAdapter extends RecyclerView.Adapter<NewsViewHolder> {
    private Context context ;
    private List<Produits> postList;
    private List<Produits> list;
    Retrofit  retrofit;
    RetrofitInterface requestInterface;

    public NewAdapter(Context context, List<Produits> apiObjects){
        this.context=context;
        this.postList = apiObjects;

    }
    public void setPostList(List<Produits> postList) {
        this.postList = postList;
        this.list = postList;
        notifyDataSetChanged();
    }

    /**
     * @param parent   parent.
     * @param viewType type.
     * @return
     */
    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new NewsViewHolder(view);
    }


    /**
     * @param holder   holder.
     * @param position pos.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull final NewsViewHolder holder,
                                 final int position) {

        final Produits produit = postList.get(position);
        final String idProduit = produit.getId();
        holder.libelle.setText(produit.getLibelle());
        holder.prix.setText(produit.getPrix());
        holder.quantite.setText(produit.getQuantite());
        holder.categorie.setText(produit.getCategorie());
        holder.mainrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UpdateProduitActivity.class);
                intent.putExtra("produit", produit);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getRootView().getContext())
                        .setMessage("Voulez-vous vraiment supprimer ce produit ?")
                        .setCancelable(false);
                alertDialog.setPositiveButton("Oui",new DialogInterface.OnClickListener(){
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String id = produit.getId();
                        delete(id);
                        Intent intent = new Intent(context, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                });

                alertDialog.setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                alertDialog.show();
            }
        });


    }


    /**
     * @return item.
     */
    @Override
    public int getItemCount() {
        if(postList != null)
            return postList.size();

        return 0;
    }
    public void delete(String idProduit) {
        String id = idProduit;
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        requestInterface = retrofit.create(RetrofitInterface.class);
        Call<ServerResponse> response = requestInterface.delete(idProduit);
        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {
                ServerResponse resp = response.body();

                if (resp.getResult().equalsIgnoreCase("success")) {
                     Toast.makeText(context, "data deleted successfully", Toast.LENGTH_SHORT).show();
                }
                else {
                      Toast.makeText(context, "product not exist", Toast.LENGTH_SHORT).show();

                }

            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.d("failed", t.getLocalizedMessage());
                Toast.makeText(context, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

}
