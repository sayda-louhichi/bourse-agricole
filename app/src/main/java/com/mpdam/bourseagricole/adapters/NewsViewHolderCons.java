package com.mpdam.bourseagricole.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mpdam.bourseagricole.R;


class NewsViewHolderCons extends RecyclerView.ViewHolder {

    public TextView libelle;
    public TextView prix;
    public TextView quantite;
    public TextView categorie;
    public ImageView delete ;
    public LinearLayout mainrow;


    public NewsViewHolderCons(final View itemView) {
        super(itemView);
        libelle = (TextView) itemView.findViewById(R.id.libelle);
        prix = (TextView) itemView.findViewById(R.id.prix);
        //quantite = (TextView) itemView.findViewById(R.id.quantite);
        categorie = (TextView) itemView.findViewById(R.id.categorie);
        mainrow = (LinearLayout) itemView.findViewById(R.id.mainrow);
        delete = (ImageView) itemView.findViewById(R.id.delete);
    }
    public LinearLayout getMainrow() {
        return mainrow;
    }
}

