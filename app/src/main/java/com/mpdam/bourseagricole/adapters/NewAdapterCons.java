package com.mpdam.bourseagricole.adapters;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mpdam.bourseagricole.R;
import com.mpdam.bourseagricole.activities.HomeActivity;
import com.mpdam.bourseagricole.activities.UpdateProduitActivity;
import com.mpdam.bourseagricole.models.Produits;
import com.mpdam.bourseagricole.models.ServerResponse;
import com.mpdam.bourseagricole.webservices.RetrofitInterface;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mpdam.bourseagricole.webservices.APIUtil.BASE_URL;

/**
 * adapter.
 */
public class NewAdapterCons extends RecyclerView.Adapter<NewsViewHolderCons> {
    private Context context ;
    Retrofit  retrofit;
    RetrofitInterface requestInterface;
    private List<Produits> postList;
    private List<Produits> list;
    private Produits produit;

    public NewAdapterCons(Context context, List<Produits> apiObjects){
        this.context=context;
        this.postList = apiObjects;

    }
    public void setPostList(List<Produits> postList) {
        this.postList = postList;
        this.list = postList;
        notifyDataSetChanged();
    }

    /**
     * @param parent   parent.
     * @param viewType type.
     * @return
     */
    @NonNull
    @Override
    public NewsViewHolderCons onCreateViewHolder(@NonNull final ViewGroup parent, final int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cons, parent, false);

        return new NewsViewHolderCons(view);
    }

    /**
     * @param holder   holder.
     * @param position pos.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull final NewsViewHolderCons holder,
                                 final int position) {
        final Produits produit = postList.get(position);
        holder.libelle.setText(produit.getLibelle());
        holder.prix.setText(produit.getPrix());
        //holder.quantite.setText(produit.getQuantite());
        holder.categorie.setText(produit.getCategorie());
//        holder.mainrow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View view) {
//
//                Intent intent = new Intent(context, UpdateProduitActivity.class);
//                intent.putExtra("produit", produit);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                context.startActivity(intent);
//
//
//            }
//        });

//        holder.delete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View view) {
//                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(view.getRootView().getContext())
//                        .setMessage("Voulez-vous vraiment supprimer ce produit ?")
//                        .setCancelable(false);
//                alertDialog.setPositiveButton("Oui",new DialogInterface.OnClickListener(){
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                String id = produit.getId();
//                               // delete(id);
//                                Intent intent = new Intent(context,HomeActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                context.startActivity(intent);
//
//                            }
//                        });
//
//                        alertDialog.setNegativeButton("Non", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.cancel();
//                            }
//                        });
//                alertDialog.show();
//            }
//        });

    }


    /**
     * @return item.
     */
    @Override
    public int getItemCount() {
        if(postList != null)
            return postList.size();

        return 0;
    }
}
