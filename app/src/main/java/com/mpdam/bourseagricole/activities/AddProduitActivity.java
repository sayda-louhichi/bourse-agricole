package com.mpdam.bourseagricole.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.mpdam.bourseagricole.R;
import com.mpdam.bourseagricole.models.ServerResponse;
import com.mpdam.bourseagricole.webservices.RetrofitInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mpdam.bourseagricole.webservices.APIUtil.BASE_URL;

public class AddProduitActivity extends AppCompatActivity {
    String userid = "9";
    String nameuser = "sayda";
    Button save;
    EditText lib, pr,  qte;
    String radio , quantity, variable ;
    RadioButton fruits, legumes,kg,pu;
    public String libelle, prix, categorie, quantite;
    private AutoCompleteTextView country;
    private String[] articles = {"Fraise","Kiwi","Pomme de terre", "Tomate","Banane","concombre","Ananas","Poivron"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_produit);
        save = (Button) findViewById(R.id.save);
        pr = (EditText) findViewById(R.id.pr);
        qte = (EditText) findViewById(R.id.qte);
        prix = pr.getText().toString();
        // categorie = cat.getText().toString();
        quantite = qte.getText().toString();
        fruits = findViewById(R.id.fruits);
        legumes = findViewById(R.id.legumes);
        kg = findViewById(R.id.kg);
        pu = findViewById(R.id.pu);
        country =(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView);
        ArrayAdapter adapterCountries
                = new ArrayAdapter(this,android.R.layout.simple_list_item_1,articles);

        country.setAdapter(adapterCountries);
        ImageView image = findViewById(R.id.drop);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                country.showDropDown();
            }
        });
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (kg.isChecked()) {
                    variable = qte.getText().toString() +" "+ "Kg";
                } else{
                    variable = qte.getText().toString() +" "+  "Pu";
                }

                save(country.getText().toString(),pr.getText().toString()+""+"DT", variable,  radio, "9", "sayda");

            }
        });

    }

    public void onRadioButtonClicked(View view) {
        // Check to see if a button has been clicked.
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked.
        switch(view.getId()) {
            case R.id.fruits:
                if (checked)
                    radio = fruits.getText().toString();
                break;
            case R.id.legumes:
                if (checked)
                    radio = legumes.getText().toString();
                break;
            case R.id.kg:
                if (checked)
                    //quantity = kg.getText().toString();


                    break;
            case R.id.pu:
                if (checked)
                    //quantity = pu.getText().toString();

                    break;
        }
    }

    private void save(String libelle1, String prix1, String quantite1 , String categorie1, String id_user1,String name_user1) {
        String libelle = libelle1;
        String prix = prix1;
        String quantite = quantite1;
        String categorie = categorie1;
        String id_user = id_user1;
        String name_user = name_user1;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        RetrofitInterface requestInterface = retrofit.create(RetrofitInterface.class);
        Call<ServerResponse> response = requestInterface.save(libelle1,prix1,quantite1,  categorie1, id_user1, name_user1);
        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {
                ServerResponse resp = response.body();

                if (resp.getResult().equalsIgnoreCase("success")) {

                    Toast.makeText(AddProduitActivity.this, "data saved successfully", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(AddProduitActivity.this, "Invaild paramétre", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.d("failed", t.getLocalizedMessage());
                Toast.makeText(AddProduitActivity.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });



    }



}
