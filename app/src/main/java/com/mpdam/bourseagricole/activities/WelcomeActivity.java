package com.mpdam.bourseagricole.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.mpdam.bourseagricole.R;

public class WelcomeActivity extends AppCompatActivity {
    Button agriculteur, consommateur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        agriculteur = findViewById(R.id.btn_agri);
        consommateur = findViewById(R.id.btn_consom);
        agriculteur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });
        consommateur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeActivity.this, LoginConsActivity.class);
                startActivity(intent);
            }
        });
}
}
