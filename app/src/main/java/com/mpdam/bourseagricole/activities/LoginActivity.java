package com.mpdam.bourseagricole.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mpdam.bourseagricole.R;
import com.mpdam.bourseagricole.models.User;
import com.mpdam.bourseagricole.models.WsCommunicator;

import org.springframework.http.HttpMethod;

import static com.mpdam.bourseagricole.webservices.APIUtil.BASE_URL;

public class LoginActivity extends AppCompatActivity {
    EditText adr;
    EditText pwd;
    Button btn;
    TextView mdp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btn = findViewById((R.id.btn_login));
        adr = findViewById(R.id.et_email);
        pwd = findViewById(R.id.et_password);
        mdp = findViewById(R.id.txt);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                User u = new User();
                u.setEmail(adr.getText().toString());
                u.setPassword(pwd.getText().toString());
                new ConnectionTask(u).execute();

            }
        });

        ImageView image = (ImageView)findViewById(R.id.imageView);
        Animation animation1 =
                AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.blink);
        image.startAnimation(animation1);
    }
    private class ConnectionTask extends AsyncTask<Void,Void,Integer>
    {
        User u;

        public ConnectionTask(User u) {
            this.u = u;
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            return WsCommunicator.getObject(BASE_URL+"api/CheckUser", HttpMethod.POST,u,Integer.class);
        }

        @Override
        protected void onPostExecute(Integer i) {
            try{
                if(i!=1)
                {
                    SharedPreferences sp = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
                    SharedPreferences.Editor editor = sp.edit();
                    editor.putInt("livid",i);
                    editor.apply();
                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(intent);
                }
                else {
                    Toast.makeText(LoginActivity.this, "PWD Error", Toast.LENGTH_LONG).show();
                }
            }
            catch (Exception e)
            {
                Toast.makeText(LoginActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();

            }

        }
    }
    public void onBackPressed() {
        Intent intent = new Intent(LoginActivity.this,WelcomeActivity.class);
        startActivity(intent);
    }
}
