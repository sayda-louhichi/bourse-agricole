package com.mpdam.bourseagricole.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.SearchView;

import com.mpdam.bourseagricole.R;
import com.mpdam.bourseagricole.adapters.NewAdapter;
import com.mpdam.bourseagricole.models.Produits;
import com.mpdam.bourseagricole.webservices.APIUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private RecyclerView recyclerView;
    int userid;
    private Calendar calendar;
    public static NewAdapter adapter;
    private SharedPreferences sharedPreferences;
    private List<Produits> currlist = new ArrayList<>();
    private List<Produits> tasks = new ArrayList<>();
    public static SearchView searchView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        toolbar.setTitle("Liste des produits");
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(HomeActivity.this,AddProduitActivity.class);
                startActivity(intent);
            }
        });
        recyclerView=(RecyclerView)findViewById(R.id.recycle) ;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this) ;
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
        userid = sharedPreferences.getInt("livid", -1);
        APIUtil.getServiceClass().getAllPost("9").enqueue(new Callback<List<Produits>>() {
            @Override
            public void onResponse(Call<List<Produits>> call, Response<List<Produits>> response) {
                if(response.isSuccessful()){
                    List<Produits> postList = response.body();
                    currlist = postList ;
                    NewAdapter adapter = new NewAdapter(getApplicationContext(),postList);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<List<Produits>> call, Throwable t) {
                Log.d("Tag", "erreur");
            }
        });
        //NAVIGATION
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        if (drawer != null) {
            drawer.addDrawerListener(toggle);
        }
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (navigationView != null) {
            navigationView.setNavigationItemSelectedListener(this);
        }
        //search
        searchView = findViewById(R.id.searchnom);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                adapter.setPostList(filterSearch(s));
                adapter.notifyDataSetChanged();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                adapter.setPostList(filterSearch(s));
                adapter.notifyDataSetChanged();
                return true;
            }
        });
    }

    private List<Produits> filterSearch(String s) {
        List<Produits> gets = new ArrayList<Produits>();
        for(Produits g : currlist)
        {
            if(g.getLibelle().toLowerCase().contains(s.toLowerCase()))
            {
                gets.add(g);
            }
        }
        return gets;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        // Handle navigation view item clicks here.
        switch (item.getItemId()) {
            case R.id.nav_camera:
                // Handle the camera import action (for now display a toast).
                drawer.closeDrawer(GravityCompat.START);
//                Intent mainIntent  = new Intent(MainActivity.this,MainActivity.class);
//                startActivity(mainIntent);
                return true;
            case R.id.nav_gallery:
                // Handle the gallery action (for now display a toast).
                drawer.closeDrawer(GravityCompat.START);
//                Intent profilIntent  = new Intent(MainActivity.this,ProfilActivity.class);
//                startActivity(profilIntent);
                return true;
            case R.id.nav_slideshow:
                // Handle the slideshow action (for now display a toast).
                drawer.closeDrawer(GravityCompat.START);
               // displayToast(getString(R.string.chose_slideshow));
                return true;
            case R.id.nav_manage:
                // Handle the tools action (for now display a toast).
                drawer.closeDrawer(GravityCompat.START);
               // displayToast(getString(R.string.chose_tools));
                return true;
            case R.id.nav_share:
                // Handle the share action (for now display a toast).
                drawer.closeDrawer(GravityCompat.START);
//                Intent AboutIntent  = new Intent(MainActivity.this,AboutActivity.class);
//                startActivity(AboutIntent);
                return true;
            case R.id.nav_logout:
                // Handle the send action (for now display a toast).
                drawer.closeDrawer(GravityCompat.START);
                logOut();
                return true;
            default:
                return false;
        }
    }
    private void logOut(){
        Intent loginIntent  = new Intent(HomeActivity.this,LoginActivity.class);
        SharedPreferences sp = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.clear();
        editor.apply();
        startActivity(loginIntent);
        finish();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mInflater = this.getMenuInflater();
        mInflater.inflate(R.menu.mainmenu, menu);
        return true;
    }

    @Override
public boolean onOptionsItemSelected(MenuItem item) {
    // Handle item selection
    switch (item.getItemId()) {
        case R.id.refresh:
            Intent intent = new Intent(HomeActivity.this, HomeActivity.class);
            startActivity(intent);

            return true;
        default:
            return super.onOptionsItemSelected(item);
    }

}
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
        startActivity(intent);
    }



}
