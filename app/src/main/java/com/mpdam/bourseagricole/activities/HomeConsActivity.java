package com.mpdam.bourseagricole.activities;

import android.content.Intent;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.mpdam.bourseagricole.R;
import com.mpdam.bourseagricole.adapters.NewAdapterCons;
import com.mpdam.bourseagricole.models.Produits;
import com.mpdam.bourseagricole.webservices.APIUtil;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeConsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_cons);
        recyclerView=(RecyclerView)findViewById(R.id.recycle) ;
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this) ;
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        APIUtil.getServiceClass().getAllProduits().enqueue(new Callback<List<Produits>>() {
            @Override
            public void onResponse(Call<List<Produits>> call, Response<List<Produits>> response) {
                if(response.isSuccessful()){
                    List<Produits> postList = response.body();
                    NewAdapterCons adapter = new NewAdapterCons(getApplicationContext(),postList);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onFailure(Call<List<Produits>> call, Throwable t) {
                Log.d("Tag", "erreur");
            }
        });
    }
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(HomeConsActivity.this,LoginConsActivity.class);
        startActivity(intent);
    }
}
