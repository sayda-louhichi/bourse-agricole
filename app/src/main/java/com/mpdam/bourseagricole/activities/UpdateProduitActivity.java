package com.mpdam.bourseagricole.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.mpdam.bourseagricole.R;
import com.mpdam.bourseagricole.models.Produits;
import com.mpdam.bourseagricole.models.ServerResponse;
import com.mpdam.bourseagricole.webservices.RetrofitInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mpdam.bourseagricole.webservices.APIUtil.BASE_URL;

public class UpdateProduitActivity extends AppCompatActivity {
    EditText prix,quantite,categorie;
    AutoCompleteTextView libelle;
    private AutoCompleteTextView article;
    private String[] articles = {"Fraise","Kiwi","Pomme de terre", "Tomate","Banane","Concombre","Ananas","Poivron"};
    private Produits produit;
    Button save;
    String id;
    String radio , quantity,variable ;
    RadioButton fruits, legumes,kg,pu;
    Retrofit  retrofit;
    RetrofitInterface requestInterface;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_produit);
        produit = (Produits) getIntent().getSerializableExtra("produit");
        id = produit.getId();
        libelle = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        prix = (EditText) findViewById(R.id.pr);
        quantite = (EditText) findViewById(R.id.qte);
        // categorie = (EditText) findViewById(R.id.cat);
        save = (Button) findViewById(R.id.save) ;
        fruits = findViewById(R.id.fruits);
        legumes = findViewById(R.id.legumes);
        kg = findViewById(R.id.kg);
        pu = findViewById(R.id.pu);
        article =(AutoCompleteTextView)findViewById(R.id.autoCompleteTextView);
        ArrayAdapter adapterCountries
                = new ArrayAdapter(this,android.R.layout.simple_list_item_1,articles);

        article.setAdapter(adapterCountries);
        ImageView image = findViewById(R.id.drop);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                article.showDropDown();
            }
        });
        libelle.setText(produit.getLibelle());
        prix.setText(produit.getPrix());
        quantite.setText(produit.getQuantite());
//        categorie.setText(produit.getCategorie());
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (kg.isChecked()) {
                    variable= quantite.getText().toString() +" "+ "Kg";
                } else{
                    variable = quantite.getText().toString() +" "+  "Pu";
                }
                modifier(id,article.getText().toString(),prix.getText().toString()+""+"DT",variable,radio);
                Intent intent = new Intent(UpdateProduitActivity.this, HomeActivity.class);
                startActivity(intent);
            }
        });

    }
    private void modifier(String id1,String libelle1,String prix1,String quantite1, String categorie1) {
        String libelle = libelle1;
        String id = id1;
        String prix = prix1;
        String quantite = quantite1;
        String categorie = categorie1;
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        requestInterface = retrofit.create(RetrofitInterface.class);
        Call<ServerResponse> response = requestInterface.modifier(id1,libelle1,prix1,quantite1,categorie1);
        response.enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, retrofit2.Response<ServerResponse> response) {
                ServerResponse resp = response.body();

                if (resp.getResult().equalsIgnoreCase("success")) {
                    Toast.makeText(UpdateProduitActivity.this, "product changed successfully", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(UpdateProduitActivity.this, "Invaild paramétre", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.d("failed", t.getLocalizedMessage());
                Toast.makeText(UpdateProduitActivity.this, t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }
    public void onRadioButtonClicked(View view) {
        // Check to see if a button has been clicked.
        boolean checked = ((RadioButton) view).isChecked();
        // Check which radio button was clicked.
        switch(view.getId()) {
            case R.id.fruits:
                if (checked)
                    radio = fruits.getText().toString();
                break;
            case R.id.legumes:
                if (checked)
                    radio = legumes.getText().toString();
                break;
            case R.id.kg:
                if (checked)
                    //quantity = kg.getText().toString();


                    break;
            case R.id.pu:
                if (checked)
                    //quantity = pu.getText().toString();

                    break;
        }
    }
}
