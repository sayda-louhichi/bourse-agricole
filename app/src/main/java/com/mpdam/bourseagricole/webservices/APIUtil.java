package com.mpdam.bourseagricole.webservices;

/**
 * Class APIUtil.
 */
public final class APIUtil {
    /**
     * Const.
     */
    private APIUtil() {
    }

    /**
     * BaseURL.
     */
    public static final String BASE_URL = "http://192.168.1.4/BourseAgricole/public/";
//ADRESSE PAR DEFAUT DE L'EMULATEUR 10.0.2.2
    //192.168.137.1
    /**
     * @return getService.
     */
    public static RetrofitInterface getServiceClass() {
        return RetrofitAPI
                .getRetrofit(BASE_URL)
                .create(RetrofitInterface.class);
    }

}
