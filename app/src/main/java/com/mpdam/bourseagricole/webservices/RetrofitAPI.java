package com.mpdam.bourseagricole.webservices;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * API.
 */
public final class RetrofitAPI {
    /**
     * const.
     */
    private RetrofitAPI() {
    }

    /**
     * @param url url.
     * @return url.
     */
    public static Retrofit getRetrofit(final String url) {
        Gson gson = new GsonBuilder().setLenient().create();
        return new Retrofit.Builder().baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson)).build();
    }
}
