package com.mpdam.bourseagricole.webservices;

import com.mpdam.bourseagricole.models.Produits;
import com.mpdam.bourseagricole.models.ServerResponse;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RetrofitInterface {
    @GET("api/getProduits/{id_user}")
    Call<List<Produits>> getAllPost(@Path("id_user") String id_user);
    @GET("api/getAllProduits")
    Call<List<Produits>> getAllProduits();
    @FormUrlEncoded
    @POST("api/save")
    Call<ServerResponse> save (@Field("libelle") String libelle,
                               @Field("prix") String prix,
                               @Field("quantite") String quantite,
                               @Field("categorie") String categorie,
                               @Field("id_user") String id_user,
                               @Field("name_user") String name_user);
    @FormUrlEncoded
    @POST("api/modifier")
    Call<ServerResponse> modifier (@Field("id") String id,
                                    @Field("libelle") String libelle,
                                   @Field("prix") String prix,
                                   @Field("quantite") String quantite,
                                   @Field("categorie") String categorie);
    @FormUrlEncoded
    @POST("api/deleteproduit")
    Call<ServerResponse> delete (@Field("id") String id);
}
